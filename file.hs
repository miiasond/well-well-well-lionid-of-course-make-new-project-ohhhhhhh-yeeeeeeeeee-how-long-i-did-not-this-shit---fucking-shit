countLetters str c = length $ filter (== c) str

letters = ['a'..'z']

mapper string = map (\x -> (x, countLetters string x)) letters

reducer list = foldr clearer [] list

clearer (_, 0) result = result
clearer (a, b) result = [(a, b)] ++ result